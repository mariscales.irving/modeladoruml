import React, {Component} from 'react';

import './sidebar.css'

import boundary from '../assets/boundary.svg'
import control from '../assets/control.svg'
import entity from '../assets/entity.svg'
import note from '../assets/note.svg'
import text from '../assets/text.svg'
import user from '../assets/user.svg'
import line from '../assets/line.svg'
import arrow from '../assets/arrow.svg'

export default class Sidebar extends Component{
    render() {
        let lists = [];
        let elements = [];

        let listsInfo = [
            {
                'name': 'Objects',
                'list':[{'img':entity, 'name':'Entity'},  {'img':boundary, 'name':'Boundary'}, {'img':control, 'name':'Control'}]
            },{
                'name': 'Actors',
                'list':[{'img':user, 'name':'User'}]
            },{
                'name': 'Associations',
                'list':[{'img':line, 'name':'Line'}, {'img':arrow, 'name':'Arrow'}]
            },{
                'name': 'Annotations',
                'list':[{'img':note, 'name':'Note'}, {'img':text, 'name':'Text'}]
            },
        ];

        for (let i in listsInfo){
            elements = [];
            for (let j in listsInfo[i].list){
                elements.push(
                    <label>
                        <div className="element">
                            <img className="element-icon nodrag" src={listsInfo[i].list[j].img}/>
                            {listsInfo[i].list[j].name}
                        </div>
                    </label>
                )
            }
            lists.push(
                <div>
                    <div className="list-name">{listsInfo[i].name}</div>
                    <div className="elements">
                        {elements}
                    </div>
                </div>
            )
        }

        return (
            <div id="sidebar" className="noselect">
                {lists}
            </div>
        );
    }
}